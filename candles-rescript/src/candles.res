let birthday_cake_candles = candles => {
  open Js.Array2

  let highest = reduce(candles, (x, y) => x > y ? x : y, 0)
  reduce(candles, (acc, candle) => candle == highest ? acc + 1 : acc)
}

[3, 1, 2, 3]
  ->birthday_cake_candles
  ->Js.log
