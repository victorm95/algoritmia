let staircase n =
  let rec gen acc x chr =
    if x = 0 then acc
    else gen (chr ^ acc) (x - 1) chr in
  let rec loop acc x =
    if x = (n + 1) then acc
    else
      let hash = gen "" x "#" in
      let space = gen hash (n - x) " " in
      loop (acc ^ space ^ "\n") (x + 1) in
  loop "" 1

let () =
  4
  |> staircase
  |> print_endline
