let smallest_int lst =
  let rec loop acc =
    if List.mem acc lst then loop (acc + 1)
    else acc
  in
    loop 1

let () =
  let input = [1; 3; 6; 4; 1; 2] in
  let result = smallest_int input in
  print_endline (string_of_int result)
