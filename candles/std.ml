let () =
  let candles = [3; 1; 2; 3;] in
  let highest = List.fold_left max 0 candles in
  let highest_candles = List.filter ((=) highest) candles in
  let result = List.length highest_candles in
  print_endline (string_of_int result)
