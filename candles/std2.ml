let birthday_cacke_candles candles =
  let tallest = List.fold_left max 0 candles in
  let sum_tallest acc candle = if candle = tallest then acc + 1 else acc in
    List.fold_left sum_tallest 0 candles

let () =
  [3; 1; 2; 3]
    |> birthday_cacke_candles
    |> string_of_int
    |> print_endline
