let tallest_candles candles =
  let rec loop acc current l =
    match l with
    | [] -> acc
    | x :: xs ->
      if x > current then loop 1 x xs else
      if x = current then loop (acc + 1) x xs
      else loop acc current xs in
  loop 0 0 candles

let () =
  [3; 1; 2; 3]
    |> tallest_candles
    |> string_of_int
    |> print_endline
