let tallest lst =
  let rec loop acc l =
    match l with
    | [] -> acc
    | x :: xs -> if x > acc then loop x xs else loop acc xs
  in
    loop 0 lst


let count el lst =
  let rec loop acc l =
    match l with
    | [] -> acc
    | x :: xs -> if x = el then loop (acc + 1) xs else loop acc xs
  in
    loop 0 lst


let birthday_cake_candles candles =
  count (tallest candles) candles


let () =
  let candles = [3; 2 ; 1; 3] in
  let result = birthday_cake_candles candles in
  print_endline (string_of_int result)
