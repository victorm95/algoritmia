package solution_test

import (
	"palindromic/solution"
	"testing"
)

func TestGeneratePalindromicNumber(t *testing.T) {
	cases := []struct {
		Input  string
		Output string
	}{
		{"8199", "989"},
		{"88199", "98189"},
		{"888199", "98889"},
		{"39878", "898"},
		{"00900", "9"},
		{"0000", "0"},
		{"54321", "5"},
		{"0055", "5005"},
		{"666", "666"},
	}

	for _, _case := range cases {
		t.Run(_case.Input, func(t *testing.T) {
			if res := solution.GeneratePalindromicNumber(_case.Input); res != _case.Output {
				t.Fatalf("'%s' expected to be '%s' but got '%s'", _case.Input, _case.Output, res)
			}
		})
	}
}
