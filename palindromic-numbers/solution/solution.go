package solution

import (
	"sort"
	"strings"
)

// sort.Slice(nums, func(i, j int) bool {
// 	return nums[i] > nums[j]
// })

type tuple struct {
	n string
	x int
}

func GeneratePalindromicNumber(number string) string {
	nums := strings.Split(number, "")
	count := make(map[string]int)

	for _, n := range nums {
		if _, exists := count[n]; !exists {
			count[n] = 0
		}
		count[n] = count[n] + 1
	}

	tuples := make([]tuple, len(count))

	i := 0
	for k, v := range count {
		tuples[i] = tuple{k, v}
		i = i + 1
	}

	sort.Slice(tuples, func(i, j int) bool {
		return tuples[i].x > tuples[j].x
	})

	// a := make([]tuple, 0)

	// a = append(a, tuples[0])

	// even := tuples[0].x % 2 == 0
	// for i := 1; i < len(tuples); i++ {
	// 	tupl := tuples[i]

	// 	if even && tupl.x == 1 {
	// 		a = append(a, tupl)
	// 		break
	// 	}

	// 	if !even
	// }

	generate(tuples)

	return ""
}

// [
// 	{n "9" x 0}
// 	{n "8" x 1}
// 	{n "7" x 1}
// ]
// 1
// -1
// 9 9

func generate(nums []tuple) string {
	var result string
	var writes int
	var single int = -1
	var i = 0

	for {

	}

	return result
}

func isPalindrom(num string) bool {
	return false
}
